package com.devcamp.s10.rainbow_restapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RainbowControl {
    @CrossOrigin
    @GetMapping("/rainbow")
    public ArrayList<String> getList(){
        ArrayList<String> listArr = new ArrayList<>();

        // String colors = new String[]{"red", "orange", "yellow", "green", "blue", "indigo", "violet" };
        String red = "red";
        String orange = "orange";
        String yellow = "yellow";
        String green = "green";
        String blue = "blue";
        String indigo = "indigo";
        String violet = "violet";

        listArr.add(red);
        listArr.add(orange);
        listArr.add(yellow);
        listArr.add(green);
        listArr.add(blue);
        listArr.add(indigo);
        listArr.add(violet);

        return listArr;
    }
}
