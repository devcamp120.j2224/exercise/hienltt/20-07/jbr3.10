package com.devcamp.s10.rainbow_restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowRestapiApplication.class, args);
	}

}
